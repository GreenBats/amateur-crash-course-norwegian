# Amateur crash course Norwegian

30-40 minutes presentation in org-mode (org-narrow-to-subtree), to people who are going to be in Trøndelag for a while

org-file made with [Spacemacs](http://spacemacs.org/). PDF and Tex-file exported from there.